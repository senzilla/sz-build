# SPDX-License-Identifier: ISC

. package/info

usage() {
cat <<EOF
Usage: $0 [OPTION...]

Options:

--help			this help message
--prefix=PREFIX		install prefix [/]
--bindir=DIR		user executables [PREFIX/bin]
--sbindir=DIR		system executables [PREFIX/sbin]
--libexecdir=DIR	package executables [PREFIX/libexec]
--includedir=DIR	include files [PREFIX/include]
--sharedir=DIR		shared data files [PREFIX/share]
--mandir=DIR		manual pages [PREFIX/share/man]
--shebangdir=DIR	absolute path for #! invocations [BINDIR]
--enable-slashpackage	enable the /package policy [disabled]

If no --prefix is given, the following applies;
includedir is /usr/include, sharedir is /usr/share, mandir is /usr/share/man
EOF

exit 0
}

#
# Global paths
#

prefix=
bindir='$prefix/bin'
sbindir='$prefix/sbin'
libexecdir='$prefix/libexec'
includedir='$prefix/include'
sharedir='$prefix/share'
mandir='$prefix/share/man'
shebangdir='$bindir'
slashpackage=

#
# Package paths
#

package_base='$prefix'
package_prefix='$prefix'
package_bindir='$bindir'
package_sbindir='$sbindir'
package_libexecdir='$libexecdir'
package_sharedir='$sharedir/$package'
package_mandir='$mandir'

#
# Options
#

for arg
do
	case "$arg" in
	--help) usage;;
	--prefix=*) prefix=${arg#*=};;
	--bindir=*) bindir=${arg#*=};;
	--sbindir=*) sbindir=${arg#*=};;
	--libexecdir=*) libexecdir=${arg#*=};;
	--includedir=*) includedir=${arg#*=};;
	--sharedir=*) sharedir=${arg#*=};;
	--mandir=*) mandir=${arg#*=};;
	--shebangdir=*) shebangdir=${arg#*=};;
	--enable-slashpackage) slashpackage=y;;
	--disable-slashpackage) slashpackage=;;

	# XXX: Since we are compatibile with the skaware build system
	# we silently ignore other options that are not applicable.
	--with-*) ;;
	--enable-*) ;;
	--disable-*) ;;

	*) echo "$0: unknown option $arg";;
	esac
done

# Add /usr in the default case.
if test -z "$prefix" -o "$prefix" = "/"
then
	if test "$libexecdir" = '$prefix/libexec'
	then
		libexecdir=/usr/libexec
	fi
	if test "$includedir" = '$prefix/include'
	then
		includedir=/usr/include
	fi
	if test "$sharedir" = '$prefix/share'
	then
		sharedir=/usr/share
	fi
	if test "$mandir" = '$prefix/share/man'
	then
		mandir=/usr/share/man
	fi
fi

if test x$slashpackage = xy
then
	bindir=$prefix/command
	sbindir=$bindir
	libexecdir=$bindir
	shebangdir=/command
	package_base=$prefix/package/$category/$package
	package_prefix=${package_base}-${version}
	package_bindir=$package_prefix/command
	package_sbindir=$package_bindir
	package_libexecdir=$package_bindir
	package_includedir=$package_prefix/include
	package_sharedir=$package_prefix/share
	package_mandir=$package_prefix/man
fi

#
# Platform commands
#

# BSD and Unix-like defaults.
cmd_cd='$bindir/execline-cd'
cmd_chroot='/usr/sbin/chroot'
cmd_env='/usr/bin/env'
cmd_ftp='/usr/bin/ftp -o -'
cmd_gzip='/usr/bin/gzip -n'
cmd_install='/usr/bin/install'
cmd_sha256='/bin/sha256 -q'
cmd_tar='/bin/tar'
cmd_tar_i='$cmd_tar -I'

case $(uname -s) in
Darwin)
	cmd_tar='/usr/bin/tar'
	cmd_ftp='/usr/bin/curl -sL'
	cmd_sha256='/usr/bin/shasum -a256'
	;;

Linux)
	cmd_ftp='/usr/bin/wget -qO-'
	cmd_sha256='/usr/bin/sha256sum'
	cmd_tar_i='$cmd_tar -T'
	;;
esac

#
# Path expansion
#

paths="bindir
sbindir
libexecdir
includedir
sharedir
mandir
shebangdir
package_base
package_prefix
package_bindir
package_sbindir
package_libexecdir
package_sharedir
package_mandir
cmd_cd
cmd_chroot
cmd_env
cmd_ftp
cmd_gzip
cmd_install
cmd_sha256
cmd_tar
cmd_tar_i"

prefix=${prefix%/}
for i in $paths
do
	eval x=\"\${$i}\"
	eval $i=\"$x\"
done

redirfd -w 1 policy.env cat <<EOF
# This file was generated with: $0 $@

# Global paths
PREFIX=$prefix
BINDIR=$bindir
SBINDIR=$sbindir
LIBEXECDIR=$libexecdir
INCLUDEDIR=$includedir
SHAREDIR=$sharedir
MANDIR=$mandir
SHEBANGDIR=$shebangdir
SLASHPACKAGE=$slashpackage

# Package paths
PACKAGE_NAME=$package
PACKAGE_VERSION=$version
PACKAGE_BASE=$package_base
PACKAGE_PREFIX=$package_prefix
PACKAGE_BINDIR=$package_bindir
PACKAGE_SBINDIR=$package_sbindir
PACKAGE_LIBEXECDIR=$package_libexecdir
PACKAGE_INCLUDEDIR=$package_includedir
PACKAGE_SHAREDIR=$package_sharedir
PACKAGE_MANDIR=$package_mandir

# Platform commands
CMD_CD=$cmd_cd
CMD_CHROOT=$cmd_chroot
CMD_ENV=$cmd_env
CMD_FTP=$cmd_ftp
CMD_GZIP=$cmd_gzip
CMD_INSTALL=$cmd_install
CMD_SHA256=$cmd_sha256
CMD_TAR=$cmd_tar
CMD_TAR_I=$cmd_tar_i

# Variable index
POLICY=PREFIX \\
BINDIR \\
SBINDIR \\
LIBEXECDIR \\
INCLUDEDIR \\
SHAREDIR \\
MANDIR \\
SHEBANGDIR \\
SLASHPACKAGE \\
PACKAGE_NAME \\
PACKAGE_PREFIX \\
PACKAGE_BINDIR \\
PACKAGE_SBINDIR \\
PACKAGE_LIBEXECDIR \\
PACKAGE_SHAREDIR \\
PACKAGE_MANDIR \\
CMD_CD \\
CMD_CHROOT \\
CMD_ENV \\
CMD_FTP \\
CMD_GZIP \\
CMD_INSTALL \\
CMD_SHA256 \\
CMD_TAR \\
CMD_TAR_I
EOF
