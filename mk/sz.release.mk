# SPDX-License-Identifier: ISC

POLICY?=	policy.env

-include package/info

RELEASE_SOURCE?=	.
RELEASE_KEY?=		~/.ssh/id_ed25519
RELEASE_HOST?=		${host}
RELEASE_PATH?=		pub/${PACKAGE_NAME}/source

_RVER=		${PACKAGE_NAME}-${PACKAGE_VERSION}
_RTAR=		${_RVER}.tar.gz
_RTMP=		/tmp/${_RVER}
_RDIR=		${_RTMP}/${PACKAGE_NAME}
_RSHA=		${_RTMP}/SHA256
_RPKG=		${_RTMP}/${_RTAR}
_RURL=		ftp://${RELEASE_HOST}/${RELEASE_PATH}

.PHONY: release
release: policy.env ${_RPKG} ${_RSHA}.local

.PHONY: release-clean
release-clean:
	rm -rf ${_RTMP}

${_RDIR}/package/plist: package/plist
	sz-copy ${_RDIR}
	sz-preprocess -pm 0644 Makefile ${_RDIR}/Makefile
	sz-preprocess -pm 0755 configure ${_RDIR}/configure

${_RPKG}: ${_RDIR}/package/plist
	sed 's/^/${PACKAGE_NAME}\//' $> | \
		${CD} ${@D} redirfd -w 1 $@ sz-archive -f -

${_RSHA}.local: ${_RPKG}
	${CD} ${@D} redirfd -w 1 $@ sz-sha256 ${_RTAR}

${_RSHA}.remote: ${_RSHA}.local
	redirfd -w 1 $@ ${FTP} ${_RURL}/SHA256 || ${INSTALL} -m 0644 $> $@

${_RSHA}.remote.sig: ${_RSHA}.remote
	redirfd -w 1 $@ ${FTP} ${_RURL}/SHA256.sig || sz-sign -k ${RELEASE_KEY} $>
	sz-sign -v $@

${_RSHA}.new: ${_RSHA}.local ${_RSHA}.remote ${_RSHA}.remote.sig
	${INSTALL} -m 0644 ${_RSHA}.remote $@
	redirfd -r 0 ${_RSHA}.local sz-fensure $@

${_RSHA}.new.sig: ${_RSHA}.new
	sz-sign -k ${RELEASE_KEY} $>

.PHONY: publish
publish: policy.env ${_RPKG} ${_RSHA}.new ${_RSHA}.new.sig
	echo "cd ${RELEASE_PATH}\n\
	put ${_RSHA}.new.sig SHA256.sig\n\
	put ${_RSHA}.new SHA256\n\
	put ${_RPKG} ${_RTAR}" | \
		ftp ${RELEASE_HOST}
