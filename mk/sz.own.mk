# SPDX-License-Identifier: ISC

BUILD?=		build

-include package/targets.mk
-include package/deps.mk
-include policy.env

PREFIX?=
BINDIR?=	/bin
SBINDIR?=	/sbin
LIBEXECDIR?=	/usr/libexec
SHAREDIR?=	/usr/share
MANDIR?=	/usr/share/man
SHEBANGDIR?=	/bin
SLASHPACKAGE?=

# Portability
.CURDIR!=	pwd

# Platform commands
CMD_CD?=	execline-cd
CMD_FTP?=	ftp -o -
CMD_INSTALL?=	install

CD?=		${CMD_CD}
FTP?=		${CMD_FTP}
INSTALL?=	${CMD_INSTALL}
