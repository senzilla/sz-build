# SPDX-License-Identifier: ISC

.MAIN: all

BUILD?=		build

PP?=		sz-preprocess
INSTALL?=	sz-install

_BIN?=		${BIN_TARGETS:%=${BUILD}/bin/%}
_LIBEXEC?=	${LIBEXEC_TARGETS:%=${BUILD}/libexec/%}
_INCLUDE?=	${INCLUDE_TARGETS:%=${BUILD}/include/%}
_SHARE?=	${SHARE_TARGETS:%=${BUILD}/share/%}
_MAN?=		${MAN_TARGETS:%=${BUILD}/man/%}

.PHONY: all
all: policy.env do-build

.PHONY: pre-build
pre-build:

.PHONY: do-build
do-build: pre-build ${_BIN} ${_LIBEXEC} ${_INCLUDE} ${_SHARE} ${_MAN}

.PHONY: clean
clean:
	rm -rf ${BUILD}

.PHONY: install
install: install-bin install-libexec install-include install-share

.PHONY: install-bin
install-bin: ${BIN_TARGETS:%=${DESTDIR}${PACKAGE_BINDIR}/%}

.PHONY: install-libexec
install-libexec: ${LIBEXEC_TARGETS:%=${DESTDIR}${PACKAGE_LIBEXECDIR}/%}

.PHONY: install-include
install-include: ${INCLUDE_TARGETS:%=${DESTDIR}${PACKAGE_INCLUDEDIR}/%}

.PHONY: install-share
install-share: ${SHARE_TARGETS:%=${DESTDIR}${PACKAGE_SHAREDIR}/%}

.PHONY: global-links
global-links: ${DESTDIR}${PACKAGE_BASE} ${BIN_TARGETS:%=${DESTDIR}${PREFIX}/command/%}

${DESTDIR}${PACKAGE_BASE}: ${DESTDIR}${PACKAGE_PREFIX}
	ln -s ${PACKAGE_NAME}-${PACKAGE_VERSION} $@

.if defined(INCLUDE_TARGETS)
global-links: ${DESTDIR}${INCLUDEDIR}/${PACKAGE_NAME}

${DESTDIR}${INCLUDEDIR}/${PACKAGE_NAME}:
	ln -s ../..${PACKAGE_BASE}/include $@
.endif

.if defined(SHARE_TARGETS)
global-links: ${DESTDIR}${SHAREDIR}/${PACKAGE_NAME}

${DESTDIR}${SHAREDIR}/${PACKAGE_NAME}:
	ln -s ../..${PACKAGE_BASE}/share $@
.endif

.for b in ${_BIN}
${DESTDIR}${PREFIX}/command/${b:T}: ${DESTDIR}${PACKAGE_BINDIR}/${b:T}
	ln -s ..${PACKAGE_BASE:${PREFIX}=}/command/${b:T} $@

${DESTDIR}${PACKAGE_BINDIR}/${b:T}: ${b}
	${INSTALL} -Dm 0755 $> $@

.if !target(${b})
${b}: src/${b:T}
	${PP} $> $@
.endif
.endfor

.for l in ${_LIBEXEC}
${DESTDIR}${PACKAGE_LIBEXECDIR}/${l:T}: ${l}
	${INSTALL} -Dm 0755 $> $@

${l}: src/${l:T}
	${PP} $> $@
.endfor

.for i in ${_INCLUDE}
${DESTDIR}${PACKAGE_INCLUDEDIR}/${i:${BUILD}/include/%=%}: ${i}
	${INSTALL} -Dm 0644 $> $@

${i}: ${i:${BUILD}/%=%}
	${INSTALL} -Dm 0644 $> $@
.endfor

.for s in ${_SHARE}
${DESTDIR}${PACKAGE_SHAREDIR}/${s:${BUILD}/share/%=%}: ${s}
	${INSTALL} -Dm 0644 $> $@

${s}: ${s:${BUILD}/%=%}
	${INSTALL} -Dm 0644 $> $@
.endfor

.if defined(MAN_TARGETS)
.PHONY: install-man
install: install-man

.    for m in ${_MAN}
_MAN_TARGET=	${DESTDIR}${PACKAGE_MANDIR}/${m:T}
_LINK_TARGET=	${DESTDIR}${MANDIR}/man${m:E}/${m:T}
install-man: ${_MAN_TARGET}
global-links: ${_LINK_TARGET}

${_LINK_TARGET}: ${_MAN_TARGET}
	ln -s ../../../..${PACKAGE_BASE}/man/${m:T} $@

${_MAN_TARGET}: ${m}
	${INSTALL} -Dm 0644 $> $@

${m}: man/${m:T}
	${INSTALL} -Dm 0644 $> $@
.    endfor
.endif
