# SPDX-License-Identifier: ISC

PACKAGE_TYPE=		downstream

.MAIN: all

-include package/info

PACKAGE_NAME=		${package}
PACKAGE_VERSION=	${version}

RELEASE_CONFIGURE=	0

UPSTREAM_DIR?=		upstream
UPSTREAM_REMOTE?=	origin
UPSTREAM_MAIN?=		main
UPSTREAM_BUILD?=	build

_UGIT=			git --git-dir=${UPSTREAM_DIR}/.git --work-tree=${UPSTREAM_DIR}
_UBUILD=		${.CURDIR}/${BUILD}/.sz_build
_UPSTREAM=		${.CURDIR}/.git/modules/${UPSTREAM_DIR}/HEAD
_UPLIST=		${.CURDIR}/package/plist.upstream

.PHONY: all
all: build

.PHONY: build
build: ${_UPLIST} ${_UBUILD}

.PHONY: clean
clean:
	rm -rf ${BUILD}

.PHONY: pull-main
pull-main:
	${_UGIT} switch ${UPSTREAM_MAIN}
	${_UGIT} pull --rebase ${UPSTREAM_REMOTE} ${UPSTREAM_MAIN}

.PHONY: push-main
push-main:
	${_UGIT} push --force ${UPSTREAM_REMOTE} ${UPSTREAM_MAIN}

.PHONY: push-build
push-build:
	${_UGIT} push --force ${UPSTREAM_REMOTE} ${UPSTREAM_BUILD}

.PHONY: push-patches
push-patches:
	sz-dsort patches/ branch | cut -f2- | \
		xargs ${_UGIT} push --force ${UPSTREAM_REMOTE}

.PHONY: rebase
rebase-patches: pull-main
	sz-dsort patches/ branch upstream weight | cut -f2- | \
		sz-patch-rebase ${UPSTREAM_DIR}

.PHONY: format-patches
format-patches: pull-main
	sz-dsort patches/ branch upstream version | \
		sz-patch-format ${UPSTREAM_DIR}

.PHONY: build-patches
build-patches:
	${_UGIT} switch ${UPSTREAM_MAIN}
	${_UGIT} branch -D ${UPSTREAM_BUILD} || true
	${_UGIT} switch -c ${UPSTREAM_BUILD}
	sz-dsort patches/ version weight | sz-patch-apply ${UPSTREAM_DIR}

${_UBUILD}: ${_UPLIST}
	${CD} ${UPSTREAM_DIR} sz-copy -f $> ${@D}
	touch $@

${_UPLIST}: ${_UPSTREAM}
	${_UGIT} switch ${UPSTREAM_BUILD}
	redirfd -w 1 $@ sz-plist ${UPSTREAM_DIR}
