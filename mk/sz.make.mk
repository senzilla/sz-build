# SPDX-License-Identifier: ISC

.MAIN: install

_MSHARE=	${DESTDIR}/usr/share/mk
_MLOCAL=	${DESTDIR}/usr/local/include

_MALL=		${ALL_TARGETS:%=${_MSHARE}/%} \
		${ALL_TARGETS:%=${_MLOCAL}/%}
_MBSD=		${BSD_TARGETS:%=${_MSHARE}/%}
_MGNU=		${GNU_TARGETS:%=${_MLOCAL}/%}

# XXX: Use inference rules for portable dependencies?
.PHONY: install ${_MALL} ${_MBSD} ${_MGNU}

install: ${_MALL} ${_MBSD} ${_MGNU}

${_MALL}:
	${INSTALL} -Dm 0644 ${@F} $@

${_MBSD}:
	${INSTALL} -Dm 0644 bsd.${@F} $@

${_MGNU}:
	${INSTALL} -Dm 0644 gnu.${@F} $@
