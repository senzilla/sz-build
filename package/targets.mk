# SPDX-License-Identifier: ISC

BIN_TARGETS=		sz-chmod		\
			sz-copy			\
			sz-dsort		\
			sz-fensure		\
			sz-fvar			\
			sz-mkdir		\
			sz-mtime		\
			sz-plist		\
			sz-repro		\
			sz-sha256

BIN_TARGETS+=		sz-archive		\
			sz-install		\
			sz-preprocess		\
			sz-patch-apply		\
			sz-patch-format		\
			sz-patch-rebase		\
			sz-sign

BIN_TARGETS+=		sz-br-table

BIN_TARGETS+=		sz-rc-preprocess

LIBEXEC_TARGETS+=	sz-preprocess-file	\
			sz-preprocess-make	\
			sz-preprocess-shell	\
			sz-rc-join

INCLUDE_TARGETS=	configure.policy.sh	\
			configure.skaware.sh

MAN_TARGETS=		sz-file-policy.1
