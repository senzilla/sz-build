# sz-build – Senzilla build tools 

A suite of tools that make it easier to build and package
software according to both the traditional Filesystem Hierarchy Standard (FHS) as well
as the more novel slashpackage convention.  

## Requirements

- [POSIX system](https://pubs.opengroup.org/onlinepubs/9699919799.2018edition)

## License

[Internet Software Consortium](https://git.senzilla.io/sz-build/tree/COPYING)

## Download

See https://git.senzilla.io/sz-build 
