# SPDX-License-Identifier: ISC

.include "mk/sz.own.mk"

INSTALL=	${BUILD}/bin/sz-install
PP=		env PATH=${BUILD}/bin:$$PATH ${BUILD}/bin/sz-preprocess.bootstrap

# XXX: Bootstrap early tools to use our own build system.

_BOOTSTRAP=	${INSTALL} \
		${BUILD}/bin/sz-mtime \
		${BUILD}/bin/sz-preprocess.bootstrap \
		${BUILD}/libexec/sz-preprocess-file

_CMD=		awk '{sub("@SHEBANGDIR@", "${SHEBANGDIR}"); sub("@PACKAGE_LIBEXECDIR@", "${.CURDIR}/${BUILD}/libexec")} 1'

pre-build: ${_BOOTSTRAP}

${_BOOTSTRAP}: ${_BOOTSTRAP:T:R:%=src/%}
	@mkdir -p ${@D}
	${_CMD} src/${@:T:R} > $@
	@chmod 0755 $@

.include "mk/bsd.sz.script.mk"
.include "mk/sz.release.mk"
